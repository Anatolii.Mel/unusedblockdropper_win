﻿# Check if the current user has administrator rights
if (-not ([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole]::Administrator)) {
    Write-Host "This script requires administrator privileges to run."
    Write-Host "Please run .EXE as an administrator and try again."
    Pause
    Exit
}

try {

    $validDrive = $false
    while (-not $validDrive) {
        # Prompt the user to enter a drive letter for file creation (e.g., C)
        $driveLetter = Read-Host "Enter the drive letter to create the file (e.g., C)"
        
        # Check if only one letter is entered
        if ($driveLetter -match '^[a-zA-Z]$') {
            $validDrive = $true
        }
        else {
            Write-Host "Invalid drive letter entered. Please enter only one letter (A-Z)."
        }
    }

    # Get information about free space on the specified drive
    $driveInfo = Get-WmiObject Win32_LogicalDisk | Where-Object {$_.DeviceID -eq "${driveLetter}:"}
    $freeSpaceGB = [int]($driveInfo.FreeSpace / 1GB)

    # Calculate the file size to be 1 gigabyte less than free space on the drive
    $sizeInGB = $freeSpaceGB - 1

    # Check in case there is not enough free space to create the file
    if ($sizeInGB -le 0) {
        Write-Host "There is not enough free space on drive $driveLetter to create the file."
        Pause
        Exit
    }

    # Calculate the file size in bytes
    $sizeInBytes = [long]($sizeInGB * 1GB)  # 1GB = 1073741824 bytes

    # Construct the file path using the entered drive letter
    $filePath = "{0}:\RemoveMe.txt" -f $driveLetter

    # Create a new file of specified size using fsutil
    fsutil file createnew $filePath $sizeInBytes

    Write-Host "File 'RemoveMe.txt' of size $sizeInGB gigabytes successfully created on drive $driveLetter."

    # Remove the created file
    Remove-Item $filePath -Force

    Write-Host "File 'RemoveMe.txt' was successfully deleted."
}
catch {
    # Handle any errors that occur during script execution
    Write-Host "An error occurred: $_"
    Pause
}
